/* eslint-disable react/prop-types */
const Button = (props) => {
    const { text, backgroundColor} = props
    return (
    <button style = {{backgroundColor: backgroundColor}}>{text}</button>
)
}
export default Button
