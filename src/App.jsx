import Button from "./Components/Button/Button";

function App() {
  return (
    <div>
      <Button
        text={"Button"}
        backgroundColor={"#fff"}
      />
    </div>
  );
}

export default App;
